CREATE DATABASE if not exists hamburgueseria;
--
USE hamburgueseria;
--
create table if not exists trabajador (
id_trabajador int auto_increment primary key,
dni_trabajador varchar(9) not null,
nombre_trabajador varchar(30) not null,
apellidos_trabajador varchar(30) not null,
telefono_trabajador varchar(9) not null
);
--
create table if not exists cliente (
id_cliente int auto_increment primary key,
nombre_cliente varchar(30) not null,
apellidos_cliente varchar(30) not null,
direccion_cliente varchar(50) not null,
telefono_cliente varchar(9) not null
);
--
create table if not exists pedido (
id_pedido int auto_increment primary key,
id_cliente int,
id_trabajador int,
fecha_pedido date,
hamburguesa_pedido varchar(20),
patatas_pedido varchar(20),
bebida_pedido varchar(20),
foreign key (id_cliente) references cliente(id_cliente),
foreign key (id_trabajador) references trabajador(id_trabajador)
);
--
delimiter ||
create function existeDni(f_dni varchar(9))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(id_trabajador) from trabajador)) do
    if  ((select dni_trabajador from trabajador where id_trabajador = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeTelefonoCliente(f_telefono_cliente varchar(9))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(id_cliente) from cliente)) do
    if  ((select telefono_cliente from cliente where id_cliente = (i + 1)) like f_telefono_cliente) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;