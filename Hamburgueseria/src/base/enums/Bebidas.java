package base.enums;

public enum Bebidas {

    AGUA("Agua"),
    COCACOLA_NORMAL("Coca-Cola"),
    COCACOLA_ZERO("Coca-Cola Zero"),
    FANTA_NARANJA("Fanta Naranja"),
    FANTA_LIMON("Fanta Limón"),
    NESTEA("Nestea"),
    CERVEZA("Cerveza");

    private String valor;

    Bebidas(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
