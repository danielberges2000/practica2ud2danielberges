package base.enums;

public enum CamposCliente {

    ID_CLIENTE("id_cliente"),
    NOMBRE_CLIENTE("nombre_cliente"),
    APELLIDOS_CLIENTE("apellidos_cliente"),
    DIRECCION_CLIENTE("direccion_cliente"),
    TELEFONO_CLIENTE("telefono_cliente");

    private String valor;

    CamposCliente(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }
}
