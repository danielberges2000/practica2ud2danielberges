package base.enums;

public enum CamposPedido {

    ID_PEDIDO("id_pedido"),
    ID_CLIENTE("id_cliente"),
    ID_TRABAJADOR("id_trabajador"),
    FECHA_PEDIDO("fecha_pedido"),
    HAMBURGUESA("hamburguesa_pedido"),
    PATATAS("patatas_pedido"),
    BEBIDA("bebida_pedido");

    private String valor;

    CamposPedido(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }
}
