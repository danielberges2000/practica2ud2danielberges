package base.enums;

public enum CamposTrabajador {

    ID_CLIENTE("id_trabajador"),
    DNI_TRABAJADOR("dni_trabajador"),
    NOMBRE_TRABAJADOR("nombre_trabajador"),
    APELLIDOS_TRABAJADOR("apellidos_trabajador"),
    TELEFONO_TRABAJADOR("telefono_trabajador");

    private String valor;

    CamposTrabajador(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }
}
