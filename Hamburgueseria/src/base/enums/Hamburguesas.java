package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboTipoEditorial de la vista.
 * Representan los de eeditorial que existen que existen.
 */
public enum Hamburguesas {

    BIG_MAC("Big Mac"),
    CUARTO_DE_LIBRA("Cuarto de Libra"),
    CBO("CBO"),
    MCPOLLO("McPollo"),
    MCROYAL("McRoyale"),
    CHEESEBURGER("Cheeseburger"),
    GRAND_MACEXTREME("Grand McExtreme");

    private String valor;

    Hamburguesas(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }
}
