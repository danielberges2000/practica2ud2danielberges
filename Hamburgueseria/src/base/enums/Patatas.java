package base.enums;

public enum Patatas {

    NORMALES("Normales"),
    DELUXE("Deluxe");

    private String valor;

    Patatas(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
