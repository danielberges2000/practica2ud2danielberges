package base.enums;

public enum Tablas {

    TRABAJADORES("Trabajadores"),
    CLIENTES("Clientes"),
    PEDIDOS("Pedidos");

    private String valor;

    Tablas(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }
}
