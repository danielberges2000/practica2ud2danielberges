package gui;

import base.enums.CamposCliente;
import base.enums.CamposPedido;
import base.enums.CamposTrabajador;
import base.enums.Hamburguesas;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

/**
 * Clase Controlador
 * @author Daniel Berges
 * 16/01/22
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase Controlador
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    /**
     * Método que refresca todas las tablas
     */
    private void refrescarTodo() {
        refrescarCliente();
        refrescarPedido();
        refrescarTrabajador();
        refrescar = false;
    }

    /**
     * Método addActionListeners
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.bBuscar.addActionListener(listener);
        vista.bBuscar.setActionCommand("buscar");
        vista.bSeleccionarT.addActionListener(listener);
        vista.bSeleccionarT.setActionCommand("seleccionarT");
        vista.anadirTrabajador.addActionListener(listener);
        vista.anadirTrabajador.setActionCommand("anadirTrabajador");
        vista.modificarTrabajador.addActionListener(listener);
        vista.modificarTrabajador.setActionCommand("modificarTrabajador");
        vista.eliminarTrabajador.addActionListener(listener);
        vista.eliminarTrabajador.setActionCommand("eliminarTrabajador");
        vista.anadirCliente.addActionListener(listener);
        vista.anadirCliente.setActionCommand("anadirCliente");
        vista.modificarCliente.addActionListener(listener);
        vista.modificarCliente.setActionCommand("modificarCliente");
        vista.eliminarCliente.addActionListener(listener);
        vista.eliminarCliente.setActionCommand("eliminarCliente");
        vista.anadirPedido.addActionListener(listener);
        vista.anadirPedido.setActionCommand("anadirPedido");
        vista.modificarPedido.addActionListener(listener);
        vista.modificarPedido.setActionCommand("modificarPedido");
        vista.eliminarPedido.addActionListener(listener);
        vista.eliminarPedido.setActionCommand("eliminarPedido");
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.setActionCommand("guardarOpciones");
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.trabajadoresTabla.getSelectionModel().addListSelectionListener(this);
        vista.clientesTabla.getSelectionModel().addListSelectionListener(this);
        vista.pedidosTabla.getSelectionModel().addListSelectionListener(this);
        vista.buscarTabla.getSelectionModel().addListSelectionListener(this);
        vista.itemDesconectar.addActionListener(listener);
        vista.itemDesconectar.setActionCommand("Desconectar");
    }

    /**
     * Método addWindowListeners
     * @param listener
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Método addItemListeners
     * @param controlador
     */
    private void addItemListeners(Controlador controlador) {}

    /**
     * Método valueChanged
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.trabajadoresTabla.getSelectionModel())) {
                int row = vista.trabajadoresTabla.getSelectedRow();
                vista.txtDniTrabajador.setText((String) vista.trabajadoresTabla.getValueAt(row, 1));
                vista.txtNombreTrabajador.setText((String) vista.trabajadoresTabla.getValueAt(row, 2));
                vista.txtApellidosTrabajador.setText((String) vista.trabajadoresTabla.getValueAt(row, 3));
                vista.txtTelefonoTrabajador.setText((String) vista.trabajadoresTabla.getValueAt(row, 4));
            } else if (e.getSource().equals(vista.clientesTabla.getSelectionModel())) {
                int row = vista.clientesTabla.getSelectedRow();
                vista.txtNombreCliente.setText((String) vista.clientesTabla.getValueAt(row, 1));
                vista.txtApellidosCliente.setText((String) vista.clientesTabla.getValueAt(row, 2));
                vista.txtDireccionCliente.setText((String) vista.clientesTabla.getValueAt(row, 3));
                vista.txtTelefonoCliente.setText((String) vista.clientesTabla.getValueAt(row, 4));
            } else if (e.getSource().equals(vista.pedidosTabla.getSelectionModel())) {
                int row = vista.pedidosTabla.getSelectedRow();
                vista.cbIdClientePedido.setSelectedItem(String.valueOf(vista.pedidosTabla.getValueAt(row, 1)));
                vista.cbIdTrabajadorPedido.setSelectedItem(String.valueOf(vista.pedidosTabla.getValueAt(row, 2)));
                vista.cbHamburguesaPedido.setSelectedItem(String.valueOf(vista.pedidosTabla.getValueAt(row, 4)));
                vista.cbPatatasPedido.setSelectedItem(String.valueOf(vista.pedidosTabla.getValueAt(row, 5)));
                vista.cbBebidaPedido.setSelectedItem(String.valueOf(vista.pedidosTabla.getValueAt(row, 6)));
            }
        }
    }

    /**
     * Método actionPerformed
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                vista.panel1.setVisible(false);
                vista.itemDesconectar.setText("Conectar");
                vista.itemDesconectar.setActionCommand("Conectar");
                break;
            case "Conectar":
                modelo.conectar();
                vista.panel1.setVisible(true);
                vista.itemDesconectar.setText("Desconectar");
                vista.itemDesconectar.setActionCommand("Desconectar");
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()),
                        String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            case "anadirTrabajador":
                try {
                    if (comprobarTrabajadorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.trabajadoresTabla.clearSelection();
                    } else if (modelo.trabajadorDniYaExiste(vista.txtDniTrabajador.getText())) {
                        Util.showErrorAlert("Ese DNI ya existe");
                        vista.trabajadoresTabla.clearSelection();
                    } else {
                        modelo.insertarTrabajador(vista.txtDniTrabajador.getText(),
                                vista.txtNombreTrabajador.getText(),
                                vista.txtApellidosTrabajador.getText(),
                                vista.txtTelefonoTrabajador.getText());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.trabajadoresTabla.clearSelection();
                }
                borrarCamposTrabajadores();
                refrescarTrabajador();
                break;
            case "modificarTrabajador":
                try {
                    if (comprobarTrabajadorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.trabajadoresTabla.clearSelection();
                    } else {
                        modelo.modificarTrabajador(vista.txtDniTrabajador.getText(),
                                vista.txtNombreTrabajador.getText(),
                                vista.txtApellidosTrabajador.getText(),
                                vista.txtTelefonoTrabajador.getText(),
                                Integer.parseInt((String) vista.trabajadoresTabla.getValueAt(vista.trabajadoresTabla.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.trabajadoresTabla.clearSelection();
                }
                borrarCamposTrabajadores();
                refrescarTrabajador();
                break;
            case "eliminarTrabajador":
                modelo.borrarTrabajador(Integer.parseInt((String) vista.trabajadoresTabla.getValueAt(vista.trabajadoresTabla.getSelectedRow(), 0)));
                borrarCamposTrabajadores();
                refrescarTrabajador();
                break;
            case "anadirCliente": {
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else if (modelo.clienteTelefonoYaExiste(vista.txtTelefonoCliente.getText())) {
                        Util.showErrorAlert("Ese telefono ya existe.\n Introduce un cliente diferente");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.insertarCliente(vista.txtNombreCliente.getText(),
                                vista.txtApellidosCliente.getText(),
                                vista.txtDireccionCliente.getText(),
                                vista.txtTelefonoCliente.getText());
                        refrescarCliente();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
            }
            break;
            case "modificarCliente": {
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.modificarCliente(vista.txtNombreCliente.getText(),
                                vista.txtApellidosCliente.getText(),
                                vista.txtDireccionCliente.getText(),
                                vista.txtTelefonoCliente.getText(),
                                Integer.parseInt((String) vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                        refrescarCliente();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
            }
            break;
            case "eliminarCliente":
                modelo.borrarCliente(Integer.parseInt((String) vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                borrarCamposClientes();
                refrescarCliente();
                break;
            case "anadirPedido": {
                try {
                    if (comprobarPedidoVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.pedidosTabla.clearSelection();
                    } else {
                        modelo.insertarPedido((String) vista.cbIdClientePedido.getSelectedItem(),
                                (String) vista.cbIdTrabajadorPedido.getSelectedItem(),
                                (String) vista.cbHamburguesaPedido.getSelectedItem(),
                                (String) vista.cbPatatasPedido.getSelectedItem(),
                                (String) vista.cbBebidaPedido.getSelectedItem());
                        refrescarPedido();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.pedidosTabla.clearSelection();
                }
                borrarCamposPedidos();
            }
            break;
            case "modificarPedido": {
                try {
                    if (comprobarPedidoVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.pedidosTabla.clearSelection();
                    } else {
                        modelo.modificarPedidos((String) vista.cbIdClientePedido.getSelectedItem(),
                                (String) vista.cbIdTrabajadorPedido.getSelectedItem(),
                                (String) vista.cbHamburguesaPedido.getSelectedItem(),
                                (String) vista.cbPatatasPedido.getSelectedItem(),
                                (String) vista.cbBebidaPedido.getSelectedItem(),
                                Integer.parseInt((String) vista.pedidosTabla.getValueAt(vista.pedidosTabla.getSelectedRow(), 0)));
                        refrescarPedido();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.pedidosTabla.clearSelection();
                }
                borrarCamposPedidos();
            }
            break;
            case "eliminarPedido":
                modelo.borrarPedido(Integer.parseInt((String) vista.pedidosTabla.getValueAt(vista.pedidosTabla.getSelectedRow(), 0)));
                borrarCamposPedidos();
                refrescarPedido();
                break;
            case "seleccionarT":
                vista.cbCampo.removeAllItems();
                String valor = (String) vista.cbTabla.getSelectedItem();
                if(valor.equals("Trabajadores")) {
                    for (CamposTrabajador constant: CamposTrabajador.values()) {
                        vista.cbCampo.addItem(constant.getValor());
                    }
                    vista.cbCampo.setSelectedIndex(-1);
                } else if(valor.equals("Clientes")) {
                    for (CamposCliente constant: CamposCliente.values()) {
                        vista.cbCampo.addItem(constant.getValor());
                    }
                    vista.cbCampo.setSelectedIndex(-1);
                } else {
                    for (CamposPedido constant: CamposPedido.values()) {
                        vista.cbCampo.addItem(constant.getValor());
                    }
                    vista.cbCampo.setSelectedIndex(-1);
                }
                break;
            case "buscar":
                if(comprobarBuscarVacio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    vista.buscarTabla.clearSelection();
                } else {
                    refrescarBuscar();
                }
                break;
        }
    }

    /**
     * Método refrescar Busqueda
     */
    private void refrescarBuscar() {
        try {
            vista.buscarTabla.setModel(construirTableModeloBuscar(modelo.buscar((String) vista.cbTabla.getSelectedItem(),
                    (String) vista.cbCampo.getSelectedItem(),
                    vista.txtDescripcion.getText())));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que permite construir una tabla modelo buscar
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModeloBuscar(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmBuscar.setDataVector(data, columnNames);
        return vista.dtmBuscar;
    }

    /**
     * Método que comprueba si esta vacio
     * @return
     */
    private boolean comprobarBuscarVacio() {
        return vista.cbTabla.getSelectedIndex() == -1 ||
                vista.cbCampo.getSelectedIndex() == -1 ||
                vista.txtDescripcion.getText().isEmpty();
    }

    /**
     * Método que refresca un pedido
     */
    private void refrescarPedido() {
        try {
            vista.pedidosTabla.setModel(construirTableModelPedido(modelo.consultarPedido()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que permite construir una DefaultTableModel de Pedido
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelPedido(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmPedido.setDataVector(data, columnNames);
        return vista.dtmPedido;
    }

    /**
     * Método que refresca un Cliente
     */
    private void refrescarCliente() {
        try {
            vista.clientesTabla.setModel(construirTableModeloCliente(modelo.consultarCliente()));
            vista.cbIdClientePedido.removeAllItems();
            for (int i = 0; i < vista.dtmCliente.getRowCount(); i++) {
                vista.cbIdClientePedido.addItem(vista.dtmCliente.getValueAt(i, 0) + " - " +
                        vista.dtmCliente.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que permite construir una DefaultTableModel de Cliente
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModeloCliente(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmCliente.setDataVector(data, columnNames);
        return vista.dtmCliente;
    }

    /**
     * Método que refresca un Trabajador
     */
    private void refrescarTrabajador() {
        try {
            vista.trabajadoresTabla.setModel(construirTableModelTrabajador(modelo.consultarTrabajador()));
            vista.cbIdTrabajadorPedido.removeAllItems();
            for (int i = 0; i < vista.dtmTrabajador.getRowCount(); i++) {
                vista.cbIdTrabajadorPedido.addItem(vista.dtmTrabajador.getValueAt(i, 0) + " - " +
                        vista.dtmTrabajador.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que permite construir una DefaultTableModel de Trabajador
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelTrabajador(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmTrabajador.setDataVector(data, columnNames);
        return vista.dtmTrabajador;

    }

    /**
     * Método setDataVector
     * @param rs
     * @param columnCount
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Método setOptions
     */
    private void setOptions() {
    }

    /**
     * Método borrarCamposTrabajador
     */
    private void borrarCamposTrabajadores() {
        vista.txtDniTrabajador.setText("");
        vista.txtNombreTrabajador.setText("");
        vista.txtApellidosTrabajador.setText("");
        vista.txtTelefonoTrabajador.setText("");
    }

    /**
     * Método borrarCamposClientes
     */
    private void borrarCamposClientes() {
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.txtDireccionCliente.setText("");
        vista.txtTelefonoCliente.setText("");
    }

    /**
     * Método borrarCamposPedidos
     */
    private void borrarCamposPedidos() {
        vista.cbIdClientePedido.setSelectedIndex(-1);
        vista.cbIdTrabajadorPedido.setSelectedIndex(-1);
        vista.cbHamburguesaPedido.setSelectedIndex(-1);
        vista.cbPatatasPedido.setSelectedIndex(-1);
        vista.cbBebidaPedido.setSelectedIndex(-1);
    }

    /**
     * Método comprobarTrabajadorVacio
     * @return
     */
    private boolean comprobarTrabajadorVacio() {
        return vista.txtDniTrabajador.getText().isEmpty() ||
                vista.txtNombreTrabajador.getText().isEmpty() ||
                vista.txtApellidosTrabajador.getText().isEmpty() ||
                vista.txtTelefonoTrabajador.getText().isEmpty();
    }

    /**
     * Método comprobarClienteVacio
     * @return
     */
    private boolean comprobarClienteVacio() {
        return vista.txtNombreCliente.getText().isEmpty() ||
                vista.txtApellidosCliente.getText().isEmpty() ||
                vista.txtDireccionCliente.getText().isEmpty() ||
                vista.txtTelefonoCliente.getText().isEmpty();
    }

    /**
     * Método comprobarPedidoVacio
     * @return
     */
    private boolean comprobarPedidoVacia() {
        return vista.cbIdClientePedido.getSelectedIndex() == -1 ||
                vista.cbIdTrabajadorPedido.getSelectedIndex() == -1 ||
                vista.cbHamburguesaPedido.getSelectedIndex() == -1 ||
                vista.cbPatatasPedido.getSelectedIndex() == -1 ||
                vista.cbBebidaPedido.getSelectedIndex() == -1;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
