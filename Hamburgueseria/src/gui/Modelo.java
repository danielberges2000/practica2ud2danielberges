package gui;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import util.Util;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;
/**
 * Clase Modelo
 * @author Daniel Berges
 * 16/01/22
 */
public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Constructor de la clase Modelo
     */
    public Modelo() {
        getPropValues();
    }

    private Connection conexion;

    /**
     * Método que conecta con la base de datos
     */
    void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/hamburgueseria", user, password);
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/", user, password);
                PreparedStatement statement = null;
                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    /**
     * Método que permite leer el Fichero hamburgueseria_java.sql
     * @return stringBuilder
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("hamburgueseria_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    /**
     * Método permite desconectar con la base de datos
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que inserta un Cliente a la base de datos
     * @param nombre
     * @param apellidos
     * @param direccion
     * @param telefono
     */
    void insertarCliente(String nombre, String apellidos, String direccion, String telefono) {
        String sentenciaSql = "INSERT INTO cliente (nombre_cliente, apellidos_cliente, direccion_cliente, telefono_cliente)" + "VALUES (?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setString(3, direccion);
            sentencia.setString(4, telefono);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que inserta un pedido a la base de datos
     * @param cliente
     * @param trabajador
     * @param hamburguesa
     * @param patatas
     * @param bebida
     */
    void insertarPedido(String cliente, String trabajador, String hamburguesa, String patatas, String bebida) {
        LocalDate localDate = LocalDate.now();
        Date date = Date.valueOf(localDate);
        String sentenciaSql = "INSERT INTO pedido (id_cliente, id_trabajador, fecha_pedido, hamburguesa_pedido, patatas_pedido, bebida_pedido) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        int idCliente = Integer.valueOf(cliente.split(" ")[0]);
        int idTrabajador = Integer.valueOf(trabajador.split(" ")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idCliente);
            sentencia.setInt(2, idTrabajador);
            sentencia.setDate(3, date);
            sentencia.setString(4, hamburguesa);
            sentencia.setString(5, patatas);
            sentencia.setString(6, bebida);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que inserta un trabajador a la base de datos
     * @param dni
     * @param nombre
     * @param apellidos
     * @param telefono
     */
    void insertarTrabajador(String dni, String nombre, String apellidos, String telefono) {
        String sentenciaSql = "INSERT INTO trabajador (dni_trabajador, nombre_trabajador, apellidos_trabajador, telefono_trabajador) " + "VALUES (?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, dni);
            sentencia.setString(2, nombre);
            sentencia.setString(3, apellidos);
            sentencia.setString(4, telefono);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que modifica un cliente de la base de datos
     * @param nombre
     * @param apellidos
     * @param direccion
     * @param telefono
     * @param id_autor
     */
    void modificarCliente(String nombre, String apellidos, String direccion, String telefono, int id_autor) {
        String sentenciaSql = "UPDATE cliente SET nombre_cliente=?, apellidos_cliente=?, direccion_cliente=?, telefono_cliente=?" + "WHERE id_cliente=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setString(3, direccion);
            sentencia.setString(4, telefono);
            sentencia.setInt(5, id_autor);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que modifica un pedido de la base de datos
     * @param cliente
     * @param trabajador
     * @param hamburguesa
     * @param patatas
     * @param bebida
     * @param id_pedido
     */
    void modificarPedidos(String cliente, String trabajador, String hamburguesa, String patatas, String bebida, int id_pedido) {
        String sentenciaSql = "UPDATE pedido SET id_cliente=?, id_trabajador=?, hamburguesa=?, patatas=?, bebida=?" + "WHERE id_pedido=?";
        PreparedStatement sentencia = null;
        int idCliente = Integer.valueOf(cliente.split(" ")[0]);
        int idTrabajador = Integer.valueOf(trabajador.split(" ")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idCliente);
            sentencia.setInt(2, idTrabajador);
            sentencia.setString(3, hamburguesa);
            sentencia.setString(4, patatas);
            sentencia.setString(5, bebida);
            sentencia.setInt(6, id_pedido);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que modifica un trabajador de la base de datos
     * @param dni
     * @param nombre
     * @param apellidos
     * @param telefono
     * @param id_trabajador
     */
    void modificarTrabajador(String dni, String nombre, String apellidos, String telefono, int id_trabajador) {
        String sentenciaSql = "UPDATE trabajador SET dni_trabajador=?, nombre_trabajador=?, apellidos_trabajador=?, telefono_trabajador=? WHERE id_trabajador = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, dni);
            sentencia.setString(2, nombre);
            sentencia.setString(3, apellidos);
            sentencia.setString(4, telefono);
            sentencia.setInt(5, id_trabajador);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
    }

    /**
     * Métdo que elimina un pedido de la base de datos
     * @param id_pedido
     */
    void borrarPedido(int id_pedido) {
        String sentenciaSql = "DELETE FROM pedido WHERE id_pedido=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_pedido);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que elimina un cliente de la base de datos
     * @param id_cliente
     */
    void borrarCliente(int id_cliente) {
        String sentenciaSql = "DELETE FROM cliente WHERE id_cliente=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_cliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            Util.showErrorAlert("No se puede borrar un trabajador con un pedido");
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que elimina un trabajador de la base de datos
     * @param id_trabajador
     */
    void borrarTrabajador(int id_trabajador) {
        String sentenciaSql = "DELETE FROM trabajador WHERE id_trabajador=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_trabajador);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            Util.showErrorAlert("No se puede borrar un trabajador con un pedido");
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que consulta un pedido de la base de datos
     * @return
     * @throws SQLException
     */
    ResultSet consultarPedido() throws SQLException {
        String sentenciaSql = "SELECT concat(p.id_pedido) AS 'ID', concat(l.id_cliente, ' - ', l.nombre_cliente) AS 'ID CLIENTE'," +
                "concat(t.id_trabajador, ' - ', t.nombre_trabajador) AS 'ID TRABAJADOR', concat(p.fecha_pedido) AS 'FECHA',concat(p.hamburguesa_pedido) AS 'HAMBURGUESA'," +
                "concat(p.patatas_pedido) AS 'PATATAS', concat(p.bebida_pedido) AS 'BEBIDA' FROM pedido AS p " +
                "INNER JOIN cliente AS l ON l.id_cliente = p.id_cliente " +
                "INNER JOIN trabajador AS t ON t.id_trabajador = p.id_trabajador";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método que consulta un cliente de la base de datos
     * @return
     * @throws SQLException
     */
    ResultSet consultarCliente() throws SQLException {
        String sentenciaSql = "SELECT concat(id_cliente) AS 'ID', concat(nombre_cliente) AS 'NOMBRE', concat(apellidos_cliente) AS 'APELLIDOS'," +
                "concat(direccion_cliente) AS 'DIRECCIÓN', concat(telefono_cliente) AS 'TELEFONO' FROM cliente";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método que consulta un trabajador de la base de datos
     * @return
     * @throws SQLException
     */
    ResultSet consultarTrabajador() throws SQLException {
        String sentenciaSql = "SELECT concat(id_trabajador) AS 'ID', concat(dni_trabajador) AS 'DNI', concat(nombre_trabajador) AS 'NOMBRE', " +
                "concat(apellidos_trabajador) AS 'APELLIDOS', concat(telefono_trabajador) AS 'TELÉFONO' FROM trabajador";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método getPropValues
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método setPropValues
     * @param ip
     * @param user
     * @param pass
     * @param adminPass
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip=ip;
        this.user=user;
        this.password=pass;
        this.adminPassword=adminPass;
    }

    /**
     * Método que comprueba si el dni de un trabajador existe
     * @param dni_trabajador
     * @return
     */
    public boolean trabajadorDniYaExiste(String dni_trabajador) {
        String consultaDni = "SELECT existeDni(?)";
        PreparedStatement function;
        boolean dniExists = false;
        try {
            function = conexion.prepareStatement(consultaDni);
            function.setString(1,dni_trabajador);
            ResultSet rs = function.executeQuery();
            rs.next();
            dniExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dniExists;
    }

    /**
     * Método que comprueba si el teléfono de un cliente existe
     * @param telefono_cliente
     * @return
     */
    public boolean clienteTelefonoYaExiste(String telefono_cliente) {
        String consultaTelefono = "SELECT existeTelefonoCliente(?)";
        PreparedStatement function;
        boolean telefonoExists = false;
        try {
            function = conexion.prepareStatement(consultaTelefono);
            function.setString(1, telefono_cliente);
            ResultSet rs = function.executeQuery();
            rs.next();
            telefonoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return telefonoExists;
    }

    /**
     * Método que realiza una busqueda de una tabla
     * @param tabla
     * @param campo
     * @param busqueda
     * @return
     * @throws SQLException
     */
    ResultSet buscar(String tabla, String campo, String busqueda) throws SQLException {
        String sentenciaSql = "";
        busqueda = "'" + busqueda + "'";
        if(tabla.equals("Trabajadores")) {
            sentenciaSql = "SELECT id_trabajador AS 'ID', dni_trabajador AS 'DNI', nombre_trabajador AS 'NOMBRE', " +
                    "apellidos_trabajador AS 'APELLIDOS', telefono_trabajador AS 'TELÉFONO' FROM trabajador WHERE " + campo + " = " + busqueda;
        }else if(tabla.equals("Clientes")) {
            sentenciaSql = "SELECT id_cliente AS 'ID', nombre_cliente AS 'NOMBRE', apellidos_cliente AS 'APELLIDOS'," +
                    "direccion_cliente AS 'DIRECCIÓN', telefono_cliente AS 'TELEFONO' FROM cliente WHERE " + campo + " = " + busqueda;
        }else {
            sentenciaSql = "SELECT id_pedido AS 'ID', id_cliente AS 'ID CLIENTE'," +
                    "id_trabajador AS 'ID TRABAJADOR', fecha_pedido AS 'FECHA', hamburguesa_pedido AS 'HAMBURGUESA'," +
                    "patatas_pedido AS 'PATATAS', bebida_pedido AS 'BEBIDA' FROM pedido WHERE " + campo + " = " + busqueda;
        }
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

}