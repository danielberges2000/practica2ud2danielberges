package gui;

import base.enums.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
/**
 * Clase Vista
 * @author Daniel Berges
 * 16/01/22
 */
public class Vista extends JFrame {

    private final static String TITULOFRAME = "Hamburguesería";
    private JTabbedPane tabbedPane;
    JPanel panel1;
    private JPanel JPanelLibro;
    private JPanel JPanelAutor;
    private JPanel JPanelEditorial;

    //TRABAJADOR
    JTextField txtTitulo;
    JComboBox comboAutor;
    JComboBox comboEditorial;
    JComboBox comboGenero;
    DatePicker fecha;
    JTextField txtIsbn;
    JTextField txtPrecioLibro;
    JTable trabajadoresTabla;
    JButton anadirTrabajador;
    JButton modificarTrabajador;
    JButton eliminarTrabajador;

    //CLIENTE
    JTextField txtNombreCliente;
    JTextField txtApellidosCliente;
    JTextField txtTelefonoCliente;
    JTable clientesTabla;
    JButton eliminarCliente;
    JButton anadirCliente;
    JButton modificarCliente;
    JComboBox cbHamburguesaPedido;
    JTable pedidosTabla;
    JButton eliminarPedido;
    JButton anadirPedido;
    JButton modificarPedido;

    JTextField txtDniTrabajador;
    JTextField txtNombreTrabajador;
    JTextField txtApellidosTrabajador;
    JTextField txtTelefonoTrabajador;
    JTextField txtDireccionCliente;
    JComboBox cbBebidaPedido;
    JComboBox cbPatatasPedido;
    JComboBox cbIdClientePedido;
    JComboBox cbIdTrabajadorPedido;
    JComboBox cbTabla;
    JButton bBuscar;
    JTable buscarTabla;
    JComboBox cbCampo;
    JTextField txtDescripcion;
    JButton bSeleccionarT;
    private JPanel JPanelEdi;
    DefaultTableModel dtmPedido;
    DefaultTableModel dtmCliente;
    DefaultTableModel dtmTrabajador;
    DefaultTableModel dtmBuscar;

    //BARRA MENU
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    //CUADRO DIALOGO
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase Vista
     * LLamo al método initFrame
     */
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Método que inicia los componentes
     */
    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+300,this.getHeight()+40));
        this.setLocationRelativeTo(this);
        optionDialog=new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Método que establece los DefaultTableModel a las tablas
     */
    private void setTableModels() {
        this.dtmTrabajador =new DefaultTableModel();
        this.trabajadoresTabla.setModel(dtmTrabajador);
        this.dtmCliente =new DefaultTableModel();
        this.clientesTabla.setModel(dtmCliente);
        this.dtmPedido =new DefaultTableModel();
        this.pedidosTabla.setModel(dtmPedido);
        this.dtmBuscar = new DefaultTableModel();
        this.buscarTabla.setModel(dtmBuscar);
    }

    /**
     * Método que establece el Menu con sus opciones
     */
    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones= new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar= new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir=new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Método que establece los valores a los combobox
     */
    private void setEnumComboBox() {
        for (Tablas constant: Tablas.values()) {
            cbTabla.addItem(constant.getValor());
        }
        cbTabla.setSelectedIndex(-1);
        cbCampo.setSelectedIndex(-1);
        for (Hamburguesas constant: Hamburguesas.values()) {
            cbHamburguesaPedido.addItem(constant.getValor());
        }
        cbHamburguesaPedido.setSelectedIndex(-1);
        for (Patatas constant: Patatas.values()) {
            cbPatatasPedido.addItem(constant.getValor());
        }
        cbPatatasPedido.setSelectedIndex(-1);
        for (Bebidas constant: Bebidas.values()) {
            cbBebidaPedido.addItem(constant.getValor());
        }
        cbBebidaPedido.setSelectedIndex(-1);
    }

    /**
     * Método setAdminDialog
     */
    private void setAdminDialog() {
        btnValidate= new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword= new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100,26));
        Object[] options = new Object[] {adminPassword,btnValidate};
        JOptionPane jop =new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,options);
        adminPasswordDialog=new JDialog(this,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

}
