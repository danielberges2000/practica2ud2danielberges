package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;
/**
 * Clase Principal
 * @author Daniel Berges
 * 16/01/22
 */
public class Principal {
    /**
     * Creo un objeto de la clase Vista
     * Creo un objeto de la clase Modelo
     * Creo un objeto de la clase Controlador enviando los objetos como parámetros al constructor
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
