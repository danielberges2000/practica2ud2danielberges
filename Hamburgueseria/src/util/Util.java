package util;

import javax.swing.JOptionPane;
/**
 * Clase Util
 * @author Daniel Berges
 * 16/01/22
 */
public class Util {
    /**
     * Método showErrorAlert
     */
    public static void showErrorAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    /**
     * Método showWarningAlert
     */
    public static void showWarningAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Aviso", JOptionPane.WARNING_MESSAGE);
    }
    /**
     * Método showInfoAlert
     */
    public static void showInfoAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Información", JOptionPane.INFORMATION_MESSAGE);
    }
}
